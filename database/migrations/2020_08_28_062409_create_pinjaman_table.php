<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pinjaman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('buku_id');
            $table->unsignedBigInteger('mahasiswa_id');
            $table->dateTime('tanggal_pinjam');
            $table->dateTime('tanggal_batas');
            $table->dateTime('tanggal_pengembalian')->nullable();
            $table->boolean('on_time')->nullable();

            $table->foreign('mahasiswa_id')
                ->references('id')
                ->on('users');
            $table->foreign('buku_id')
                ->references('id')
                ->on('bukus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pinjaman');
    }
}
