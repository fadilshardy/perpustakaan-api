<?php

Route::namespace ('Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::namespace ('Mahasiswa')->middleware('auth:api')->group(function () {
    Route::post('create-new-mahasiswa', 'MahasiswaController@store');
    Route::patch('update-data-mahasiswa/{mahasiswa}', 'MahasiswaController@update');
    Route::delete('delete-data-mahasiswa/{mahasiswa}', 'MahasiswaController@destroy');
});

Route::namespace ('Buku')->middleware('auth:api')->group(function () {
    Route::post('create-new-buku', 'BukuController@store');
    Route::patch('update-data-buku/{buku}', 'BukuController@update');
    Route::delete('delete-data-buku/{buku}', 'BukuController@destroy');

});

Route::namespace ('Pinjaman')->middleware('auth:api')->group(function () {
    Route::post('create-new-pinjaman', 'PinjamanController@store');
});

Route::patch('update-data-pinjaman/{pinjaman}', 'Pinjaman\PinjamanController@update')->middleware(['auth', 'role']);

Route::get('mahasiswa/{mahasiswa}', 'Mahasiswa\MahasiswaController@show');
Route::get('buku/{buku}', 'Buku\BukuController@show');
Route::get('pinjaman/{pinjaman}', 'Pinjaman\PinjamanController@show');
