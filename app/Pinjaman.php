<?php

namespace App;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $guarded = [];
    protected $table = "pinjaman";

    public function user()
    {
        return $this->belongsTo(User::class, 'mahasiswa_id');
    }

    public function buku()
    {
        return $this->belongsTo(Buku::class);
    }

    public function isOnTime()
    {
        if (Carbon::now()->gt($this->tanggal_batas)) {
            return 0;
        } else {
            return 1;
        }
    }
}
