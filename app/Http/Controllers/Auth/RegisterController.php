<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        User::create([
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'role_id' => 1,
        ]);

        return response('Akun berhasil dibuat!');
    }
}
