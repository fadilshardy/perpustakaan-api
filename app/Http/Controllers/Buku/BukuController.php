<?php

namespace App\Http\Controllers\Buku;

use App\Buku;
use App\Http\Controllers\Controller;
use App\Http\Requests\BukuRequest;
use App\Http\Resources\BukuResource;
use Illuminate\Support\Facades\Request;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BukuRequest $request)
    {
        $Buku = Buku::create($this->BukuStore());

        return $Buku;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Buku $Buku)
    {
        return new BukuResource($Buku);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BukuRequest $request, Buku $Buku)
    {
        $Buku->update($this->BukuStore());

        return $Buku;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buku $Buku)
    {
        $Buku->delete();
        return response()->json('Data Buku berhasil dihapus');
    }

    public function BukuStore()
    {
        return [
            'kode_buku' => request('kode_buku'),
            'judul' => request('judul'),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => request('tahun_terbit'),
        ];
    }
}
