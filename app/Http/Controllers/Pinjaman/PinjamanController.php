<?php

namespace App\Http\Controllers\Pinjaman;

use App\Http\Controllers\Controller;
use App\Http\Requests\PinjamanRequest;
use App\Http\Resources\PinjamanResource;
use App\Pinjaman;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PinjamanRequest $request)
    {
        $Pinjaman = Pinjaman::create([
            'buku_id' => request('buku_id'),
            'mahasiswa_id' => auth::id(),
            'tanggal_pinjam' => request('tanggal_pinjam'),
            'tanggal_batas' => request('tanggal_batas'),
        ]);

        return $Pinjaman;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pinjaman $Pinjaman)
    {
        return new PinjamanResource($Pinjaman);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PinjamanRequest $request, Pinjaman $Pinjaman)
    {
        $Pinjaman->update([
            'buku_id' => request('buku_id'),
            'mahasiswa_id' => auth::id(),
            'tanggal_pinjam' => request('tanggal_pinjam'),
            'tanggal_batas' => request('tanggal_batas'),
            'tanggal_pengembalian' => Carbon::now(),
            'on_time' => $Pinjaman->isOnTime(),
        ]);

        return $Pinjaman;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Pinjaman $Pinjaman)
    // {
    //     $Pinjaman->delete();
    //     return response()->json('Data Pinjaman berhasil dihapus');
    // }
}
